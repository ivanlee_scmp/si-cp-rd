var page = require('webpage').create();
var system = require('system');
var args = system.args;
page.viewportSize = { width: 900, height: 1200 };
page.paperSize = { format: 'A4', orientation:'portrait', margin: '0cm' };
var uri='https://www.cpjobs.com/hk.php/myprofile/profilepreview?';
var env=args[1];
var template=args[2];
var token=args[3];
if(env=='dev'){
    uri='http://devhk.cpjobs.com/hk.php/myprofile/profilepreview?';
}
if(env=='stg'){
    uri='http://stgmyjobs.cpjobs.com/hk.php/myprofile/profilepreview?';
}
if(env=='local'){
    uri='http://www.cp.com/hk.php/myprofile/profilepreview?';
}
if(env=='ali'){
    uri='https://aliwww.cpjobs.com/hk.php/myprofile/profilepreview?';
}
uri+="template="+template+"&token="+token;
var candidateid=token.split("-")[1];
//console.log(uri);
page.open(uri, function (status) {
    if ( status === "success" ) {
        page.render('cv/cv_'+candidateid+'.pdf' , {format: 'pdf', quality: '100' });
        console.log("success"); 
        phantom.exit();
     } else {
       console.log("Page failed to load."); 
       phantom.exit();
    }
   // phantom.exit();
});