var page = require('webpage').create();
var system = require('system');
var args = system.args;
page.viewportSize = { width: 900, height: 1200 };
page.paperSize = { format: 'A4', orientation:'portrait', margin: '0cm' };
var uri='https://www.cpjobs.com/hk.php/myprofile/linkedinprofilepreview?';
var env=args[1];
var token=args[2];
if(env=='dev'){
    uri='http://devhk.cpjobs.com/hk.php/myprofile/linkedinprofilepreview?';
}
if(env=='stg'){
    uri='http://stgmyjobs.cpjobs.com/hk.php/myprofile/linkedinprofilepreview?';
}
if(env=='local'){
    uri='http://www.cp.com/hk.php/myprofile/linkedinprofilepreview?';
}
if(env=='ali'){
    uri='http://aliwww.cpjobs.com/hk.php/myprofile/linkedinprofilepreview?';
}
uri+="token="+token;
var candidateid=token.split("-")[1];
//console.log(uri+"------------------------------------------------------------");
// phantom.exit();
page.open(uri, function (status) {
    console.log(status);
    if ( status === "success" ) {
        page.render('cv/lk_'+candidateid+'.pdf' , {format: 'pdf', quality: '100' });
        console.log("success"); 
        phantom.exit();
     } else {
       console.log("Page failed to load."); 
       phantom.exit();
    }
   // phantom.exit();
});