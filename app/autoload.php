<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/** @var ClassLoader $loader */
$loader = require __DIR__.'/../vendor/autoload.php';
if (!function_exists('Aliyun_Log_Client')) {
    require_once __DIR__.'/../vendor/Aliyun/Log/Client.php';
    $loader->add('', __DIR__.'/../vendor/Aliyun/Log');
}
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
