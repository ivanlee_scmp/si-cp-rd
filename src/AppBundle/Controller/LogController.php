<?php

namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
class LogController extends Controller
{
    private $endpoint = 'cn-shenzhen.log.aliyuncs.com';
    private $accessKeyId = 'nFD6lGvnfx7L6G75';
    private $accessKey = 'kx8pBgRCmOR6ZkwRl4NeAQlIQW56GZ';
    private $project = 'cpjobs';
    private $logstore = 'candidate';
    private $token = "";
    private $salt='A3#%425ahG';
    public function cpjobsAction(Request $request)
    {
        if(!$request->get("data")){
            exit();
        }
        $param_str=  base64_decode($request->get("data"));
        parse_str(urldecode($param_str),$params);
        $default_keys=array("event","action","type","uri","value","jsondata","uid","key","useragent");
        $contents=array();
        $contents['event']=isset($params['event']) ? $params['event'] : false;
        $contents['action']=isset($params['action']) ? $params['action'] : false;
        if(!$contents['event'] || !$contents['action']){
            print_r("Parameter [event|action] is required");
            exit();
        }
        if(isset($params['key'])){
            $contents['key']=$params['key'];
        }else{
            $contents['key']=isset($params['type']) ? $params['type'] : "";
        }
        $contents['uri']=isset($params['uri']) ? $params['uri'] : $request->headers->get('referer');
        $contents['uid']="";
        if(isset($params['uid'])){
            $uid=$this->decrypt($params['uid']);
            if(is_numeric($uid)){
                $contents['uid']=$uid;
            }else{
                 $contents['uid']=$params['uid'];
            }
        }
        $contents['useragent']=isset($params['useragent']) ? $params['useragent'] : "";
        if(isset($params['value'])){
            $contents['value']=  $params['value'];
        }else{
            $contents['value']=isset($params['keyword']) ? $params['keyword'] : false;
        }
        $json_arr=array();
        foreach($params as $key =>$val){
            if(!in_array($key,$default_keys)){
                $json_arr[$key]=$val;
            }
        }
        $contents['jsondata']= count($json_arr)>0 ? json_encode($json_arr) : "";
        $contents['ip']=$request->getClientIp();
        $contents['ds']=date("Ymd");
        $contents['env']="dev";
        if(strpos($contents['uri'],"www.cpjobs.com")!==false){
            $contents['env']="prod";
        }
        if($this->container->getParameter('kernel.environment')=='dev'){
            print_r($contents);
        }
        $client = new \Aliyun_Log_Client($this->endpoint, $this->accessKeyId, $this->accessKey,$this->token);
        $topic = $request->get("topic","cpt_recruiter");
        $logItem = new \Aliyun_Log_Models_LogItem();
        $logItem->setTime(time());
        $logItem->setContents($contents);
        $logitems = array($logItem);
        $log_request = new \Aliyun_Log_Models_PutLogsRequest($this->project, $this->logstore, 
                $topic, null, $logitems);

        try {
            $response = $client->putLogs($log_request);
            //var_dump($response);
        } catch (Aliyun_Log_Exception $ex) {
           // var_dump($ex);
             $logger = $this->get('logger');
             $logger->critical('Alilog error!',$contents);
        } catch (Exception $ex) {
           // var_dump($ex);
            $logger = $this->get('logger');
            $logger->critical('Alilog error!',$contents);
        }
         return new Response("");
       // return $this->render('DataBundle:Default:index.html.twig');
    }
 private function decrypt($encrypted_string)
  {
    return trim(
      mcrypt_decrypt(
        MCRYPT_RIJNDAEL_128,
        $this->salt,
        $this->hex2bin($encrypted_string),
        MCRYPT_MODE_ECB,
        mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)
      )
    );
  }
  /**
   * invert bin2hex
   *
   * @author luffy.lai
   * @param string $data
   * @return string
   */
  protected function hex2bin($data)
  {
    $len = strlen($data);
    $newdata = null;
    for ($i = 0; $i < $len; $i+=2)
    {
      $newdata .= pack("C", hexdec(substr($data, $i, 2)));
    }
    return $newdata;
  }
}
