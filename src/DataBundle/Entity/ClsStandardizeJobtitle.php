<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClsStandardizeJobtitle
 */
class ClsStandardizeJobtitle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $jobtitle;

    /**
     * @var int
     */
    private $confidence;

    /**
     * @var int
     */
    private $deleted;

    /**
     * @var int
     */
    private $fieldId;

    /**
     * @var string
     */
    private $slug;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobtitle
     *
     * @param string $jobtitle
     * @return ClsStandardizeJobtitle
     */
    public function setJobtitle($jobtitle)
    {
        $this->jobtitle = $jobtitle;
    
        return $this;
    }

    /**
     * Get jobtitle
     *
     * @return string 
     */
    public function getJobtitle()
    {
        return $this->jobtitle;
    }

    /**
     * Set confidence
     *
     * @param integer $confidence
     * @return ClsStandardizeJobtitle
     */
    public function setConfidence($confidence)
    {
        $this->confidence = $confidence;
    
        return $this;
    }

    /**
     * Get confidence
     *
     * @return integer 
     */
    public function getConfidence()
    {
        return $this->confidence;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return ClsStandardizeJobtitle
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set fieldId
     *
     * @param integer $fieldId
     * @return ClsStandardizeJobtitle
     */
    public function setFieldId($fieldId)
    {
        $this->fieldId = $fieldId;
    
        return $this;
    }

    /**
     * Get fieldId
     *
     * @return integer 
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ClsStandardizeJobtitle
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
