<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClsSalaryIndex
 */
class ClsSalaryIndex
{
    const TYPE_POSITION = 1;
    const TYPE_PROFILE = 2;
    const TYPE_AVERAGE = 3;
    const TYPE_CORRECTED = 4;
    const TYPE_INDUSTRY = 5;
    const MIN_WAGE_HOURLY = 34.5;
    const MIN_WAGE_MONTHLY = 6210.0;  // 34.5*9*20
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $refid;

    /**
     * @var string
     */
    private $jobid;

    /**
     * @var string
     */
    private $jobtitle;

    /**
     * @var int
     */
    private $industry;

    /**
     * @var int
     */
    private $salary;

    /**
     * @var int
     */
    private $type;

    /**
     * @var int
     */
    private $experience;

    /**
     * @var \DateTime
     */
    private $createdate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refid
     *
     * @param integer $refid
     * @return ClsSalaryIndex
     */
    public function setRefid($refid)
    {
        $this->refid = $refid;
    
        return $this;
    }

    /**
     * Get refid
     *
     * @return integer 
     */
    public function getRefid()
    {
        return $this->refid;
    }

    /**
     * Set jobid
     *
     * @param string $jobid
     * @return ClsSalaryIndex
     */
    public function setJobid($jobid)
    {
        $this->jobid = $jobid;
    
        return $this;
    }

    /**
     * Get jobid
     *
     * @return string 
     */
    public function getJobid()
    {
        return $this->jobid;
    }

    /**
     * Set jobtitle
     *
     * @param string $jobtitle
     * @return ClsSalaryIndex
     */
    public function setJobtitle($jobtitle)
    {
        $this->jobtitle = $jobtitle;
    
        return $this;
    }

    /**
     * Get jobtitle
     *
     * @return string 
     */
    public function getJobtitle()
    {
        return $this->jobtitle;
    }

    /**
     * Set industry
     *
     * @param integer $industry
     * @return ClsSalaryIndex
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return integer 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set salary
     *
     * @param integer $salary
     * @return ClsSalaryIndex
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    
        return $this;
    }

    /**
     * Get salary
     *
     * @return integer 
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return ClsSalaryIndex
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set experience
     *
     * @param integer $experience
     * @return ClsSalaryIndex
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    
        return $this;
    }

    /**
     * Get experience
     *
     * @return integer 
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return ClsSalaryIndex
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;
    
        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }
}
