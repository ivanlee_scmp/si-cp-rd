<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClsCrawlSummary
 */
class ClsCrawlSummary
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $type;

    /**
     * @var int
     */
    private $jobs;

    /**
     * @var int
     */
    private $companies;

    /**
     * @var int
     */
    private $jobtitleid;

    /**
     * @var int
     */
    private $industry;

    /**
     * @var \DateTime
     */
    private $postdate;

    /**
     * @var \DateTime
     */
    private $createdate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return ClsCrawlSummary
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set jobs
     *
     * @param integer $jobs
     * @return ClsCrawlSummary
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;
    
        return $this;
    }

    /**
     * Get jobs
     *
     * @return integer 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Set companies
     *
     * @param integer $companies
     * @return ClsCrawlSummary
     */
    public function setCompanies($companies)
    {
        $this->companies = $companies;
    
        return $this;
    }

    /**
     * Get companies
     *
     * @return integer 
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Set jobtitleid
     *
     * @param integer $jobtitleid
     * @return ClsCrawlSummary
     */
    public function setJobtitleid($jobtitleid)
    {
        $this->jobtitleid = $jobtitleid;
    
        return $this;
    }

    /**
     * Get jobtitleid
     *
     * @return integer 
     */
    public function getJobtitleid()
    {
        return $this->jobtitleid;
    }

    /**
     * Set industry
     *
     * @param integer $industry
     * @return ClsCrawlSummary
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return integer 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set postdate
     *
     * @param \DateTime $postdate
     * @return ClsCrawlSummary
     */
    public function setPostdate($postdate)
    {
        $this->postdate = $postdate;
    
        return $this;
    }

    /**
     * Get postdate
     *
     * @return \DateTime 
     */
    public function getPostdate()
    {
        return $this->postdate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return ClsCrawlSummary
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;
    
        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }
}
