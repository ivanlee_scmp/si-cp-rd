<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClsIndustry
 */
class ClsIndustry
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $industrydesc;

    /**
     * @var string
     */
    private $langid;

    /**
     * @var string
     */
    private $industrykey;

    /**
     * @var string
     */
    private $desczhhk;

    /**
     * @var string
     */
    private $desczhcn;

    /**
     * @var int
     */
    private $crawlmappinggroup;

    /**
     * @var string
     */
    private $crawlmappingdesc;

    /**
     * @var string
     */
    private $crawlmappingdesczhhk;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set industrydesc
     *
     * @param string $industrydesc
     * @return ClsIndustry
     */
    public function setIndustrydesc($industrydesc)
    {
        $this->industrydesc = $industrydesc;
    
        return $this;
    }

    /**
     * Get industrydesc
     *
     * @return string 
     */
    public function getIndustrydesc()
    {
        return $this->industrydesc;
    }

    /**
     * Set langid
     *
     * @param string $langid
     * @return ClsIndustry
     */
    public function setLangid($langid)
    {
        $this->langid = $langid;
    
        return $this;
    }

    /**
     * Get langid
     *
     * @return string 
     */
    public function getLangid()
    {
        return $this->langid;
    }

    /**
     * Set industrykey
     *
     * @param string $industrykey
     * @return ClsIndustry
     */
    public function setIndustrykey($industrykey)
    {
        $this->industrykey = $industrykey;
    
        return $this;
    }

    /**
     * Get industrykey
     *
     * @return string 
     */
    public function getIndustrykey()
    {
        return $this->industrykey;
    }

    /**
     * Set desczhhk
     *
     * @param string $desczhhk
     * @return ClsIndustry
     */
    public function setDesczhhk($desczhhk)
    {
        $this->desczhhk = $desczhhk;
    
        return $this;
    }

    /**
     * Get desczhhk
     *
     * @return string 
     */
    public function getDesczhhk()
    {
        return $this->desczhhk;
    }

    /**
     * Set desczhcn
     *
     * @param string $desczhcn
     * @return ClsIndustry
     */
    public function setDesczhcn($desczhcn)
    {
        $this->desczhcn = $desczhcn;
    
        return $this;
    }

    /**
     * Get desczhcn
     *
     * @return string 
     */
    public function getDesczhcn()
    {
        return $this->desczhcn;
    }

    /**
     * Set crawlmappinggroup
     *
     * @param integer $crawlmappinggroup
     * @return ClsIndustry
     */
    public function setCrawlmappinggroup($crawlmappinggroup)
    {
        $this->crawlmappinggroup = $crawlmappinggroup;
    
        return $this;
    }

    /**
     * Get crawlmappinggroup
     *
     * @return integer 
     */
    public function getCrawlmappinggroup()
    {
        return $this->crawlmappinggroup;
    }

    /**
     * Set crawlmappingdesc
     *
     * @param string $crawlmappingdesc
     * @return ClsIndustry
     */
    public function setCrawlmappingdesc($crawlmappingdesc)
    {
        $this->crawlmappingdesc = $crawlmappingdesc;
    
        return $this;
    }

    /**
     * Get crawlmappingdesc
     *
     * @return string 
     */
    public function getCrawlmappingdesc()
    {
        return $this->crawlmappingdesc;
    }

    /**
     * Set crawlmappingdesczhhk
     *
     * @param string $crawlmappingdesczhhk
     * @return ClsIndustry
     */
    public function setCrawlmappingdesczhhk($crawlmappingdesczhhk)
    {
        $this->crawlmappingdesczhhk = $crawlmappingdesczhhk;
    
        return $this;
    }

    /**
     * Get crawlmappingdesczhhk
     *
     * @return string 
     */
    public function getCrawlmappingdesczhhk()
    {
        return $this->crawlmappingdesczhhk;
    }
}
