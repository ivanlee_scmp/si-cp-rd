<?php

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use DataBundle\Entity\ClsSalaryIndex;

class PositionController extends FOSRestController
{
    /**
     * @Rest\Post("/api/getPositionByMajor")
     */
    public function getPositionByMajorAction(Request $request){
        $studyfieldid = $request->get('studyfieldid');

        if(empty($studyfieldid)){
            return new View("studyfieldid must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }

//        $selectQuery = 'SELECT sj.id, sj.cleantitle as position, sj.industry, sj.field, sj.joblevel, COUNT(me) as confidence
//                        FROM ApiBundle:MasterEducation me
//                        JOIN ApiBundle:StandardizeJobtitle sj WHERE sj.id = me.standardizeTitleId
//                        WHERE me.studyfield = :studyfieldid
//                        GROUP BY sj.id, sj.cleantitle, sj.industry, sj.field, sj.joblevel
//                        ORDER BY confidence DESC, sj.joblevel asc';

        $selectQuery = 'SELECT * FROM (SELECT (SELECT sj2.id 
                                                 FROM standardize_jobtitle sj2 
                                                 WHERE sj2.slugtitle = sj.slugtitle AND sj2.deleted IS NOT TRUE
                                                 ORDER BY sj2.confidence DESC LIMIT 1) as id,
                                                (SELECT sj2.cleantitle 
                                                 FROM standardize_jobtitle sj2 
                                                 WHERE sj2.slugtitle = sj.slugtitle AND sj2.deleted IS NOT TRUE
                                                 ORDER BY sj2.confidence DESC LIMIT 1) as position, 
                                                (SELECT sj2.field FROM standardize_jobtitle sj2 
               			                         WHERE sj2.slugtitle = sj.slugtitle AND sj2.deleted IS NOT TRUE
               			                         ORDER BY (CASE WHEN sj2.field is null THEN 1 ELSE 0 END ) ASC, sj2.confidence DESC LIMIT 1) as field, 
                                                sj.slugtitle, 
                                                (SELECT sj2.industry FROM standardize_jobtitle sj2 
               			                         WHERE sj2.slugtitle = sj.slugtitle AND sj2.deleted IS NOT TRUE
               			                         ORDER BY (CASE WHEN sj2.industry is null THEN 1 ELSE 0 END ) ASC, sj2.confidence DESC LIMIT 1 ) as industry, count(me) as confidence
                                        FROM standardize_jobtitle sj 
                                        JOIN master_education me ON sj.id = me.standardize_title_id
                                        WHERE me.studyfield = :studyfield AND (sj.joblevel = 0 or sj.joblevel = 1) AND sj.deleted IS NOT TRUE
                                        GROUP BY sj.slugtitle
                                        ORDER BY confidence DESC LIMIT 40) AS temp 
                        WHERE temp.confidence > 20';

//        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
//            ->setMaxResults(40)
//            ->setParameter('studyfieldid', $studyfieldid)
//            ->getResult();

        $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare($selectQuery);
        $stmt->bindValue('studyfield',  $studyfieldid);
        $stmt->execute();
        $tempresult = $stmt->fetchAll();

        if (empty($tempresult)) {
            return new View("there are no related position", Response::HTTP_NOT_FOUND);
        }

        return $tempresult;
    }

    /**
     * @Rest\Post("/api/getPositionByEducation")
     */
    public function getPositionByEducationAction(Request $request){
        $educationlevel = $request->get('educationlevel');
        $major = $request->get('major');
        $studyfield = $request->get('studyfield');

        if(empty($major)){
            return new View("Major must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }
//        $selectQuery = 'SELECT sj.id, sj.cleantitle as position, sj.industry, sj.field, sj.joblevel, COUNT(me) as confidence
//                        FROM ApiBundle:MasterEducation me
//                        JOIN ApiBundle:StandardizeJobtitle sj WHERE sj.id = me.standardizeTitleId
//                        WHERE lower(me.major) like :major ';
        $selectQuery = 'SELECT * FROM (SELECT (SELECT sj2.id 
                                                FROM standardize_jobtitle sj2 
                                                WHERE sj2.slugtitle = sj.slugtitle 
                                                ORDER BY sj2.confidence DESC LIMIT 1) as id,
               			                      (SELECT sj2.cleantitle 
               			                        FROM standardize_jobtitle sj2 
               			                        WHERE sj2.slugtitle = sj.slugtitle
               			                        ORDER BY sj2.confidence DESC LIMIT 1) as position, 
               			                      (SELECT sj2.field FROM standardize_jobtitle sj2 
               			                        WHERE sj2.slugtitle = sj.slugtitle 
               			                        ORDER BY ( CASE WHEN sj2.field is null THEN 1 ELSE 0 END ) ASC, sj2.confidence DESC LIMIT 1 ) as field, 
               			                      sj.slugtitle, 
               			                      (SELECT sj2.industry FROM standardize_jobtitle sj2 
               			                         WHERE sj2.slugtitle = sj.slugtitle
               			                         ORDER BY ( CASE WHEN sj2.industry is null THEN 1 ELSE 0 END ) ASC, sj2.confidence DESC LIMIT 1 ) as industry, count(me) as confidence
                                        FROM standardize_jobtitle sj 
                                        JOIN master_education me ON sj.id = me.standardize_title_id
                                        WHERE lower(me.major) like :major AND (sj.joblevel = 0 or sj.joblevel = 1) AND sj.deleted IS NOT TRUE ';
        if($educationlevel){
            $selectQuery .= 'AND me.edulevel = :edulevel ';
        }
        if($studyfield){
            $selectQuery .= 'AND me.studyfield = :studyfield ';
        }
        $selectQuery .= 'GROUP BY sj.slugtitle 
                         ORDER BY confidence DESC LIMIT 40) AS temp 
                      WHERE temp.confidence > 20';
//        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
//            ->setMaxResults(40)
//            ->setParameter('major', '%'.$major.'%');
        $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare($selectQuery);
        $stmt->bindValue('major', '%'.strtolower($major).'%');

        if($educationlevel){
            //$tempresult->setParameter('edulevel', $educationlevel);
            $stmt->bindValue('edulevel', $educationlevel);
        }
        if($studyfield){
            //$tempresult->setParameter('studyfield', $studyfield);
            $stmt->bindValue('studyfield',  $studyfield);
        }
        //$tempresult = $tempresult->getResult();
        $stmt->execute();
        $tempresult = $stmt->fetchAll();
        if (empty($tempresult)) {
            return new View("there are no related position", Response::HTTP_NOT_FOUND);
        }
        return $tempresult;
    }

    /**
     * @Rest\Post("/api/getPositionByIndustry")
     */
    public function getPositionByIndustryAction(Request $request){
        $industryid = $request->get('industryid');

        if(empty($industryid)){
            return new View("Industry id must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }
        $selectQuery = 'SELECT *
                        FROM (SELECT (SELECT sj2.id FROM standardize_jobtitle sj2 
                                      WHERE sj2.slugtitle = sj.slugtitle AND sj2.industry = sj.industry 
                                      ORDER BY sj2.confidence DESC LIMIT 1) as id,
                                     (SELECT sj2.cleantitle FROM standardize_jobtitle sj2 
                                      WHERE sj2.slugtitle = sj.slugtitle AND sj2.industry = sj.industry 
                                      ORDER BY sj2.cleantitle NULLS LAST, sj2.confidence DESC LIMIT 1) as position,
                                     (SELECT sj2.field FROM standardize_jobtitle sj2 
                                      WHERE sj2.slugtitle = sj.slugtitle AND sj2.industry = sj.industry 
                                      ORDER BY sj2.field NULLS LAST, sj2.confidence DESC LIMIT 1) as field, sj.slugtitle, sj.industry, SUM(sj.confidence) as confidence
                        FROM standardize_jobtitle sj 
                        WHERE sj.industry = :industryid AND sj.deleted IS NOT TRUE
                        GROUP BY sj.slugtitle, sj.industry
                        ORDER BY confidence DESC) AS temp
                        WHERE temp.confidence > 20';
        $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare($selectQuery);
        $stmt->bindValue('industryid', $industryid);
        $stmt->execute();
        $tempresult = $stmt->fetchAll();
//        $selectQuery = 'SELECT sj.slugtitle, sj.industry, SUM(sj.confidence) as total
//                        FROM ApiBundle:StandardizeJobtitle sj
//                        WHERE sj.industry = :industryid
//                        GROUP BY sj.slugtitle, sj.industry
//                        ORDER BY total DESC';
//        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
//            ->setMaxResults(40)
//            ->setParameter('industryid', $industryid)
//            ->getResult();

        if (empty($tempresult)) {
            return new View("there are no related position", Response::HTTP_NOT_FOUND);
        }
        return $tempresult;
    }

    /**
     * @Rest\Post("/api/getPositionDetailsById")
     */
    public function getPositionDetailsByIdAction(Request $request){
        $positionid = $request->get('positionid');
        $nocache = $request->get('nc', 0);
        $minsalary = (float)ClsSalaryIndex::MIN_WAGE_MONTHLY;

        if(empty($positionid)){
            return new View("Position id must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }

        $selectQuery = 'SELECT sj.id, sj.cleantitle as position, sj.slugtitle, sj.industry, sj.field, sj.joblevel, sj.confidence
                        FROM ApiBundle:StandardizeJobtitle sj 
                        WHERE sj.id = :positionid ';
        $selectResult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
            ->setMaxResults(1)
            ->setParameter('positionid', $positionid)
            ->getResult();

        if(empty($selectResult)){
            return new View("Position not found", Response::HTTP_NOT_FOUND);
        }

        $slugtitle = $selectResult[0]['slugtitle'];
        $mysqlStandardizeId = $this->get('helper.common')->postgreStandardizeTitleToMysqlStandardizeId($selectResult[0]['position']);

        $selectQuery = 'SELECT sj.industry, SUM(sj.confidence) as industryConfidence
                        FROM ApiBundle:StandardizeJobtitle sj 
                        JOIN ApiBundle:StandardizeJobtitle sj1 WHERE sj1.slugtitle = sj.slugtitle
                        WHERE sj1.id = :positionid AND sj.industry is not null
                        GROUP BY sj.industry
                        ORDER BY industryConfidence DESC';
        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
            ->setMaxResults(20)
            ->setParameter('positionid', $positionid)
            ->getResult();

        $industryArray = array();
        foreach ($tempresult as $key => $value) {
            if ($value['industry']) {
                array_push($industryArray, array(
                    'id' => $value['industry'],
                    'desc' => $this->get('helper.common')->id_to_industry($value['industry']),
                    'confidence' => $value['industryConfidence']
                ));
            }
        }

        // Convert
        $salaryResult = $this->getDoctrine()->getRepository('DataBundle:ClsSalaryIndex', 'cpdb')->getIndexList($mysqlStandardizeId, true);
        $salaryArray = array(
            'results' => array(),
            'median' => $salaryResult['median'],
            'titlecount' => 0
        );

        $salaryTitleCount = $this->getDoctrine()->getManager('cpdb')->createQuery('SELECT COUNT(csi) as titlecount FROM DataBundle:ClsSalaryIndex csi WHERE csi.refid = :positionid AND (csi.type = 1 OR csi.type = 2) and csi.salary >= :minsalary')
         //   ->setParameter('positionid', $mysqlStandardizeId)
            ->setParameters(array(
                'positionid' => $mysqlStandardizeId,
                'minsalary' => $minsalary,
            ))
            ->getResult();

        $tempSalary = 0;
        foreach ($salaryResult['list'] as $key => $value){
            array_push($salaryArray['results'], array(
                'exp' => $key,
                'salary' => $value > $tempSalary  ? $value : null
            ));
            $tempSalary = $value > $tempSalary  ? $value : $tempSalary;
        }

        $salaryArray['titlecount'] = $salaryTitleCount[0]['titlecount'];
//        $educationQuery = 'SELECT sj.id, sj.cleantitle as position, sj.industry, sj.field, sj.joblevel, COUNT(me) as confidence
//                        FROM ApiBundle:MasterEducation me
//                        JOIN ApiBundle:StandardizeJobtitle sj WHERE sj.id = me.standardizeTitleId
//                        WHERE lower(me.major) like  ';

//        $educationLevelQuery = 'SELECT me.edulevel, count(me.major) as educationConfidence
//                            FROM ApiBundle:MasterEducation me
//                            WHERE me.standardizeTitleId = :positionid AND me.edulevel is not null
//                            GROUP BY me.edulevel
//                            ORDER BY educationConfidence DESC';
        $educationLevelQuery = "SELECT me.edulevel, count(me.id) as educationConfidence,
                                (CASE WHEN me.standardizeTitleId = :positionid THEN 1 ELSE 0 END) as matchSJ
                                FROM ApiBundle:MasterEducation me
                                JOIN ApiBundle:StandardizeJobtitle sj WHERE me.standardizeTitleId = sj.id 
                                WHERE (me.standardizeTitleId = :positionid OR sj.slugtitle = :slugtitle) AND me.edulevel is not null 
                                GROUP BY me.edulevel , matchSJ
                                ORDER BY matchSJ DESC, educationConfidence DESC";

        $educationLevel = $this->getDoctrine()->getManager('postgre')->createQuery($educationLevelQuery)
            ->setMaxResults(1)
            ->setParameter('positionid', $positionid)
            ->setParameter('slugtitle', $slugtitle)
            ->getResult();

        $skillsQuery = 'SELECT ms.skills, COUNT(ms.id) as skillsConfidence,
                               (CASE WHEN ms.standardizeTitleId = :standardizeid THEN 1 ELSE 0 END) as matchSJ
                        FROM ApiBundle:MasterSkills ms 
                        JOIN ApiBundle:StandardizeJobtitle sj WHERE ms.standardizeTitleId = sj.id 
                        WHERE (sj.id = :standardizeid OR sj.slugtitle = :slugtitle) AND ms.skills is not null 
                        GROUP BY ms.skills, matchSJ 
                        HAVING COUNT(ms.id) > 1
                        ORDER BY matchSJ DESC, skillsConfidence DESC ';

        $skillsResult = $this->getDoctrine()->getManager('postgre')->createQuery($skillsQuery)
            ->setMaxResults(10)
            ->setParameter('standardizeid', $positionid)
            ->setParameter('slugtitle', $slugtitle)
            ->getResult();

        $pqQuery = 'SELECT mp.pq, COUNT(mp.id) as pqConfidence,
                               (CASE WHEN mp.standardizeTitleId = :standardizeid THEN 1 ELSE 0 END) as matchSJ
                        FROM ApiBundle:MasterPq mp 
                        JOIN ApiBundle:StandardizeJobtitle sj WHERE mp.standardizeTitleId = sj.id 
                        WHERE (sj.id = :standardizeid OR sj.slugtitle = :slugtitle) AND mp.pq is not null 
                        GROUP BY mp.pq, matchSJ 
                        HAVING COUNT(mp.id) > 1
                        ORDER BY matchSJ DESC, pqConfidence DESC ';

        $pqResult = $this->getDoctrine()->getManager('postgre')->createQuery($pqQuery)
            ->setMaxResults(10)
            ->setParameter('standardizeid', $positionid)
            ->setParameter('slugtitle', $slugtitle)
            ->getResult();
//        $jobPostQuery = 'SELECT MONTH(cs.postdate) as postmonth, YEAR(cs.postdate) as postyear, count(cs.jobtitleid) as jobCount,
//                                ( SELECT count(cs2.companies)
//                                  FROM DataBundle:ClsCrawlSummary cs2
//                                  WHERE MONTH(cs2.postdate) = postmonth AND YEAR(cs2.postdate) = postyear AND cs.jobtitleid = cs2.jobtitleid ) as companyCount
//                            FROM DataBundle:ClsCrawlSummary cs
//                            WHERE cs.jobtitleid = :positionid AND cs.postdate >= :within1year
//                            GROUP BY postmonth, postyear
//                            ORDER BY postyear DESC, postmonth DESC';
        $jobPostQuery = 'SELECT MONTH(cs.postdate) as postmonth, YEAR(cs.postdate) as postyear, cs.jobs, cs.companies
                         FROM DataBundle:ClsCrawlSummary cs
                         JOIN DataBundle:ClsStandardizeJobtitle sj WHERE cs.jobtitleid = sj.id 
                         WHERE cs.jobtitleid = :positionid OR sj.slug = :slugtitle 
                         GROUP BY postmonth, postyear
                         ORDER BY postyear DESC, postmonth DESC';

//        $last12Month = new \DateTime('-12 month');
//        $last12Month->modify( 'first day of this month' );

        $jobPostResult = $this->getDoctrine()->getManager('cpdb')->createQuery($jobPostQuery)
            ->setMaxResults(12)
            ->setParameter('positionid', $mysqlStandardizeId)
            ->setParameter('slugtitle', $slugtitle)
            ->getResult();

        $jobPost = array();
        foreach ($jobPostResult as $key => $value){
            array_push($jobPost,
                array(
                    'month'=> $value['postmonth'],
                    'job' => $value['jobs'],
                    'company' => $value['companies']
                ));
        }

        $minExpQuery = "SELECT mt.prevWorkexp, count(mt) as workexpConfidence 
                        FROM ApiBundle:MasterTitle mt 
                        JOIN ApiBundle:StandardizeJobtitle sj WHERE mt.slugtitle = sj.slugtitle
                        WHERE sj.id = :standardizeid GROUP BY mt.prevWorkexp ORDER BY mt.prevWorkexp asc";
        $minExpResult = $this->getDoctrine()->getManager('postgre')->createQuery($minExpQuery)
            ->setParameter('standardizeid', $positionid)
            ->getResult();

        $minExp = null;
        foreach ($minExpResult as $key => $value){
            if($value > 10){
                $minExp = $value['prevWorkexp'];
                break;
            }
        }

        $avgExpQuery = "SELECT avg(mt.prevWorkexp) as avgExp
                        FROM ApiBundle:MasterTitle mt 
                        JOIN ApiBundle:StandardizeJobtitle sj WHERE mt.slugtitle = sj.slugtitle
                        WHERE sj.id = :standardizeid";
        $avgExpResult = $this->getDoctrine()->getManager('postgre')->createQuery($avgExpQuery)
            ->setParameter('standardizeid', $positionid)
            ->getResult();

        $avgExp = null;
        foreach ($avgExpResult as $key => $value){
            $avgExp = round($value['avgExp']);
        }

        return array(
            'position' => empty($selectResult) ? null : $selectResult[0]['position'],
            'field' => empty($selectResult) ? null : $selectResult[0]['field'],
            'joblevel' => empty($selectResult) ? null : $selectResult[0]['joblevel'],
            'educationlevel' => empty($educationLevel) ? null : $educationLevel[0]['edulevel'],
            'jobpost' => $jobPost,
            'industry' => $industryArray,
            'skills' => $skillsResult,
            'professionalqualifications' => $pqResult,
            'salary' => $salaryArray,
            'minexp' => $minExp,
            'avgExp' => $avgExp
        );
    }

    /**
     * @Rest\Post("/api/getIndustrySalary")
     */
    public function getIndustrySalaryAction(Request $request){
        $positionid = $request->get('positionid');
        $minsalary = (float)ClsSalaryIndex::MIN_WAGE_MONTHLY;
        
        if(empty($positionid)){
            return new View("Position id must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }

        $selectQuery = 'SELECT sj.industry, sj.slugtitle, SUM(sj.confidence) as industryConfidence 
                        FROM ApiBundle:StandardizeJobtitle sj 
                        JOIN ApiBundle:StandardizeJobtitle sj1 WHERE sj1.slugtitle = sj.slugtitle 
                        WHERE sj1.id = :positionid 
                        GROUP BY sj.industry, sj.slugtitle 
                        ORDER BY industryConfidence DESC';
        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($selectQuery)
            ->setMaxResults(20)
            ->setParameter('positionid', $positionid)
            ->getResult();

        $mysqlStandardizeId = $this->get('helper.common')->postgreStandardizeTitleToMysqlStandardizeId($tempresult[0]['slugtitle']);

        $resultArray = array();
        $titleCount = 0;
        foreach ($tempresult as $value) {
            $selectParams = array(
                'positionid'=>$mysqlStandardizeId,
                'minsalary'=>$minsalary,
            );
            if($value['industry'] == null){
                $selectQuery = 'SELECT (CASE 
                                        WHEN csi.experience >= 0 and csi.experience <= 1  THEN 1
                                        WHEN csi.experience >= 2 and csi.experience <= 4  THEN 2
                                        WHEN csi.experience >= 5 and csi.experience <= 7  THEN 3
                                        WHEN csi.experience >= 8 THEN 4 ELSE 0
                                    END) AS expgroup, AVG(csi.salary) as avgSalary, COUNT(csi.salary) as titleCount
                            FROM DataBundle:ClsSalaryIndex csi
                            WHERE (csi.type = 1 OR csi.type = 2) AND csi.refid=:positionid AND csi.industry is null and csi.salary > :minsalary
                            GROUP BY expgroup';
            }else{
            $selectQuery = 'SELECT (CASE 
                                        WHEN csi.experience >= 0 and csi.experience <= 1  THEN 1
                                        WHEN csi.experience >= 2 and csi.experience <= 4  THEN 2
                                        WHEN csi.experience >= 5 and csi.experience <= 7  THEN 3
                                        WHEN csi.experience >= 8 THEN 4 ELSE 0
                                    END) AS expgroup, AVG(csi.salary) as avgSalary, COUNT(csi.salary) as titleCount
                            FROM DataBundle:ClsSalaryIndex csi
                            WHERE (csi.type = 1 OR csi.type = 2) AND csi.refid=:positionid AND csi.industry=:industryid and csi.salary > :minsalary
                            GROUP BY expgroup';
                $selectParams['industryid'] = $value['industry'];
            }
//            $salaryResult = $this->getDoctrine()->getRepository('DataBundle:ClsSalaryIndex', 'cpdb')->getIndexListByIndustry($value['industry'], true);
            $salaryResult = $this->getDoctrine()->getManager('cpdb')->createQuery($selectQuery)
                ->setMaxResults(20)
                ->setParameters($selectParams)
                ->getResult();
            $salaryArray = array(
                'id' => $value['industry'] ? $value['industry'] : 0 ,
                'industry' => $value['industry'] ? $this->get('helper.common')->id_to_industry($value['industry']) : 'Not Specified',
                'salarys' => array()
            );
            $tempSalary=0;
            foreach ($salaryResult as $key => $value){
                switch ($value['expgroup']){
                    case 1:
                        $expgroup = '0-1';
                        break;
                    case 2:
                        $expgroup = '2-4';
                        break;
                    case 3:
                        $expgroup = '5-7';
                        break;
                    case 4:
                        $expgroup = '8-10';
                        break;
                    default:
                        $expgroup = '0';
                        break;
                };
                array_push($salaryArray['salarys'], array(
                    'exp' => $expgroup,
                    'salary' => round($value['avgSalary']) >= $tempSalary  ? round($value['avgSalary']) : null
                ));
                $tempSalary = round($value['avgSalary']) >= $tempSalary  ? round($value['avgSalary']) : $tempSalary;
                $titleCount += $value['titleCount'];
            }
            array_push($resultArray, $salaryArray);
        }

        return array(
            'positionid' => $positionid,
            'title' => $tempresult[0]['slugtitle'],
            'titlecount' => $titleCount,
            'results' => $resultArray
        );
    }
}