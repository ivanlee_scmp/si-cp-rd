<?php

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class PathController extends FOSRestController
{
    /**
     * @Rest\Post("/api/getPathByJobTitle")
     */
    public function getPathByJobTitleAction(Request $request) {
        $jobtitle = $request->get('jobtitle');
        $joblevel = $request->get('joblevel');
        $industry = $request->get('industry');
        //return $jobtitle;
        if(empty($jobtitle)){
            return new View("Job title must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }
        $jobtitle = $this->get('helper.common')->slugifyTitle($jobtitle);
        //$restresult = $this->getDoctrine()->getRepository('ApiBundle:TitlePath')->findAll();
        $haveCount = 'HAVING COUNT(tp) > 1 ';

        $query = 'SELECT tp.standardize_id, tp.next_standardize_id, ns.cleantitle as nextCleanTitle, ns.slugtitle, ns.industry as nextIndustry, ns.field as nextField, ns.joblevel as nextJoblevel, s.industry, s.field, s.joblevel, COUNT(tp) as pathConfidence ';
        if(!is_null($joblevel)){
            $query .= ', CASE WHEN s.joblevel = '.$joblevel.' THEN 1 ELSE 0 END as matchJoblevel ';
        }
        if(!is_null($industry)){
            $query .= ', CASE WHEN s.industry = '.$industry.' THEN 1 ELSE 0 END as matchIndustry ';
        }
        $query .= 'FROM title_path tp '.
                  'JOIN standardize_jobtitle s ON s.id = tp.standardize_id AND s.deleted is not true '.
                  'LEFT JOIN standardize_jobtitle ns ON ns.id = tp.next_standardize_id AND ns.deleted is not true '.
                  'LEFT JOIN LATERAL (SELECT aa.slugtitle, SUM(confidence) as titleconfidence '.
                  'FROM standardize_jobtitle aa '.
                  'WHERE aa.slugtitle = ns.slugtitle '.
                  'GROUP BY slugtitle) bb ON bb.slugtitle = ns.slugtitle '.
                  'WHERE s.slugtitle = :slugtitle AND bb.titleconfidence > 20 ';
        $orderby = '';
        $parameters = array('slugtitle' => $jobtitle);
        if(!is_null($joblevel)){
            $query .= 'AND (s.joblevel = '.$joblevel.' OR s.joblevel is null) ';
            $orderby .= 'matchJoblevel desc, ';
//            $parameters['joblevel1'] = (INT)$joblevel;
        }
        if(!is_null($industry)){
            $query .= 'AND (s.industry = '.$industry.' OR s.industry is null) ';
            $orderby .= 'matchIndustry desc, ';
            $haveCount = '';
        }
        $query .= 'GROUP BY tp.standardize_id, tp.next_standardize_id, ns.cleantitle, ns.slugtitle, ns.industry, ns.field, ns.joblevel, s.industry, s.field, s.joblevel '.
                  $haveCount.
                  'ORDER BY ' .$orderby. ' pathConfidence desc, nextjoblevel, nextfield, nextindustry NULLS LAST 
                  LIMIT 18';

//        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery($query)
//                            ->setParameters($parameters)
//                            ->setMaxResults(6)
//                            ->getResult();

        $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare($query);
        foreach ($parameters as $key => $value){
            $stmt->bindParam($key, $value);
        }
        $stmt->execute();
        $tempresult = $stmt->fetchAll();

        if (empty($tempresult)) {
            return new View("there are no related job title", Response::HTTP_NOT_FOUND);
        }

        return self::getPositionPathStep2($tempresult);
    }

    /**
     * @Rest\Post("/api/getPathByPositionId")
     */
    public function getPathByPositionIdAction(Request $request){
        $positionid = $request->get('positionid');
        //return $jobtitle;
        if(empty($positionid)){
            return new View("positionid must be provided", Response::HTTP_NOT_ACCEPTABLE);
        }
        //$restresult = $this->getDoctrine()->getRepository('ApiBundle:TitlePath')->findAll();
//        $tempresult = $this->getDoctrine()->getManager('postgre')->createQuery('SELECT tp.standardizeId, tp.nextStandardizeId, ns.cleantitle as nextCleanTitle, ns.industry as nextIndustry, ns.field as nextField, ns.joblevel as nextJoblevel, s.industry, s.field, s.joblevel, COUNT(tp) as pathConfidence
//                                                                        FROM ApiBundle:TitlePath tp
//                                                                        JOIN ApiBundle:StandardizeJobtitle s WHERE s.id = tp.standardizeId
//                                                                        JOIN ApiBundle:StandardizeJobtitle ns WHERE ns.id = tp.nextStandardizeId
//                                                                        WHERE tp.standardizeId = :standardizeid AND s.confidence > 20
//                                                                        GROUP BY tp.standardizeId, tp.nextStandardizeId, ns.cleantitle, ns.industry, ns.field, ns.joblevel, s.industry, s.field, s.joblevel
//                                                                        HAVING COUNT(tp) > 1
//                                                                        ORDER BY pathConfidence DESC')
//            ->setMaxResults(6)
//            ->setParameter('standardizeid', $positionid)
//            ->getResult();

        $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare('SELECT tp.standardize_id, tp.next_standardize_id, ns.cleantitle as nextCleanTitle, ns.industry as nextIndustry, ns.field as nextField, ns.joblevel as nextJoblevel, s.industry, s.field, s.joblevel, COUNT(tp) as pathConfidence, 
                                                                        (CASE WHEN tp.standardize_id = :standardizeid THEN 1 ELSE 0 END) as matchId
                                                                        FROM title_path tp
                                                                        JOIN standardize_jobtitle s ON s.id = tp.standardize_id AND s.deleted is not true
                                                                        JOIN standardize_jobtitle ns ON ns.id = tp.next_standardize_id AND ns.deleted is not true
                                                                        LEFT JOIN LATERAL (SELECT aa.slugtitle, SUM(confidence) as titleconfidence
                                                                                   FROM standardize_jobtitle aa 
                                                                                   WHERE aa.slugtitle = ns.slugtitle
                                                                                   GROUP BY aa.slugtitle) bb ON bb.slugtitle = ns.slugtitle
                                                                        WHERE (tp.standardize_id = :standardizeid OR s.slugtitle = :slugtitle) AND bb.titleconfidence > 20
                                                                        GROUP BY tp.standardize_id, tp.next_standardize_id, ns.cleantitle, ns.industry, ns.field, ns.joblevel, s.industry, s.field, s.joblevel
                                                                        HAVING COUNT(tp) > 1
                                                                        ORDER BY matchId DESC, pathConfidence DESC
                                                                        LIMIT 18');
        $stmt->bindValue('standardizeid', $positionid);
            $stmt->bindValue('slugtitle', $this->get('helper.common')->id_to_slugtitle($positionid));
            $stmt->execute();
            $tempresult = $stmt->fetchAll();
            if (empty($tempresult)) {
            return new View("there are no related job title", Response::HTTP_NOT_FOUND);
        }
        return self::getPositionPathStep2($tempresult);
    }

    function getPositionPathStep2($tempresult){
        $starttime = microtime(true);
        $restresult = array(
            'id' => $tempresult[0]['standardize_id'],
            'path' => array()
        );
        foreach ($tempresult as $key => $value){
            if($value['next_standardize_id']){
//                $nextResult = $this->getDoctrine()->getManager('postgre')->createQuery('SELECT tp.standardizeId as id, s.cleantitle, tp.nextStandardizeId, ns.cleantitle as nextCleanTitle, ns.slugtitle, ns.industry as nextIndustry, ns.field as nextField, ns.joblevel as nextJoblevel, s.industry, s.field, s.joblevel ,COUNT(tp) as pathConfidence
//                                                                        FROM ApiBundle:TitlePath tp
//                                                                        JOIN ApiBundle:StandardizeJobtitle s WHERE s.id = tp.standardizeId
//                                                                        LEFT JOIN ApiBundle:StandardizeJobtitle ns WITH ns.id = tp.nextStandardizeId AND ns.confidence > 10
//                                                                        WHERE s.id = :standardizeid
//                                                                        GROUP BY s.cleantitle, tp.standardizeId, tp.nextStandardizeId, ns.cleantitle, ns.slugtitle, ns.industry, ns.field, ns.joblevel, s.industry, s.field, s.joblevel
//                                                                        HAVING COUNT(tp) > 1
//                                                                        ORDER BY pathConfidence desc')
//                    ->setMaxResults(6)
//                    ->setParameter('standardizeid', $value['nextStandardizeId'])
//                    ->getResult();

                $stmt = $this->getDoctrine()->getManager('postgre')->getConnection()->prepare('SELECT tp.standardize_id as id, s.cleantitle, tp.next_standardize_id, ns.cleantitle as nextCleanTitle, ns.slugtitle as nextSlugTitle, ns.industry as nextIndustry, ns.field as nextField, ns.joblevel as nextJoblevel, s.industry, s.field, s.joblevel ,COUNT(tp) as pathConfidence 
                                                                        FROM title_path tp
                                                                        JOIN standardize_jobtitle s ON s.id = tp.standardize_id 
                                                                        LEFT JOIN standardize_jobtitle ns ON ns.id = tp.next_standardize_id
                                                                        LEFT JOIN LATERAL (SELECT aa.slugtitle, SUM(confidence) as titleconfidence
                                                                                   FROM standardize_jobtitle aa
                                                                                   WHERE aa.slugtitle = ns.slugtitle
                                                                                   GROUP BY slugtitle) bb ON bb.slugtitle = ns.slugtitle
                                                                        WHERE s.id = ? AND bb.titleconfidence > 20
                                                                        GROUP BY s.cleantitle, tp.standardize_id, tp.next_standardize_id, ns.cleantitle, ns.slugtitle, ns.industry, ns.field, ns.joblevel, s.industry, s.field, s.joblevel 
                                                                        HAVING COUNT(tp) > 1 
                                                                        ORDER BY pathConfidence desc 
                                                                        LIMIT 6');
                $stmt->bindValue(1, $value['next_standardize_id']);
                $stmt->execute();
                $nextResult = $stmt->fetchAll();

                $nextChild = array();
                $nextSlugTitle = array();
                foreach ($nextResult as $key2 => $value2){
                    if($value2['next_standardize_id'] != $value['standardize_id'] && $value2['nextcleantitle'] != $value['nextcleantitle'] && !in_array($value2['nextslugtitle'], $nextSlugTitle)){
                        array_push($nextChild, array(
                            'id' => $value2['next_standardize_id'],
                            'position' =>$value2['nextcleantitle'],
                            'industry' => $value2['nextindustry'],
                            'field' => $value2['nextfield'],
                            'joblevel' => $value2['nextjoblevel'],
                            'pathConfidence' => $value2['pathconfidence'],
                        ));
                        array_push($nextSlugTitle, $value2['nextslugtitle']);
                    }
//                    if($value2['nextStandardizeId'] != $value['standardizeId'] && $value2['nextCleanTitle'] != $value['nextCleanTitle']){
//                        array_push($nextChild, array(
//                            'id' => $value2['nextStandardizeId'],
//                            'position' =>$value2['nextCleanTitle'],
//                            'industry' => $value2['nextIndustry'],
//                            'field' => $value2['nextField'],
//                            'joblevel' => $value2['nextJoblevel'],
//                            'pathConfidence' => $value2['pathConfidence'],
//                        ));
//                    }
                }
                $nextResult = array(
                    'id' => $value['next_standardize_id'],
                    'position' => $value['nextcleantitle'],
                    'industry' => $value['nextindustry'],
                    'field' => $value['nextfield'],
                    'joblevel' => $value['nextjoblevel'],
                    'children' => $nextChild,
                    'pathConfidence' => $value['pathconfidence'],
                );
                array_push($restresult['path'], $nextResult);
            }
        }
        return $restresult;
    }
}
