<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasterPq
 */
class MasterPq
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $masterTitleId;

    /**
     * @var int
     */
    private $standardizeTitleId;

    /**
     * @var string
     */
    private $pq;

    /**
     * @var int
     */
    private $sourcePqId;

    /**
     * @var int
     */
    private $source;

    /**
     * @var \DateTime
     */
    private $createtime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set masterTitleId
     *
     * @param integer $masterTitleId
     * @return MasterPq
     */
    public function setMasterTitleId($masterTitleId)
    {
        $this->masterTitleId = $masterTitleId;
    
        return $this;
    }

    /**
     * Get masterTitleId
     *
     * @return integer 
     */
    public function getMasterTitleId()
    {
        return $this->masterTitleId;
    }

    /**
     * Set standardizeTitleId
     *
     * @param integer $standardizeTitleId
     * @return MasterPq
     */
    public function setStandardizeTitleId($standardizeTitleId)
    {
        $this->standardizeTitleId = $standardizeTitleId;
    
        return $this;
    }

    /**
     * Get standardizeTitleId
     *
     * @return integer 
     */
    public function getStandardizeTitleId()
    {
        return $this->standardizeTitleId;
    }

    /**
     * Set pq
     *
     * @param string $pq
     * @return MasterPq
     */
    public function setPq($pq)
    {
        $this->pq = $pq;
    
        return $this;
    }

    /**
     * Get pq
     *
     * @return string 
     */
    public function getPq()
    {
        return $this->pq;
    }

    /**
     * Set sourcePqId
     *
     * @param integer $sourcePqId
     * @return MasterPq
     */
    public function setSourcePqId($sourcePqId)
    {
        $this->sourcePqId = $sourcePqId;
    
        return $this;
    }

    /**
     * Get sourcePqId
     *
     * @return integer 
     */
    public function getSourcePqId()
    {
        return $this->sourcePqId;
    }

    /**
     * Set source
     *
     * @param integer $source
     * @return MasterPq
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return integer
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set createtime
     *
     * @param \DateTime $createtime
     * @return MasterPq
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    
        return $this;
    }

    /**
     * Get createtime
     *
     * @return \DateTime 
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }
}
