<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasterTitle
 */
class MasterTitle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $source;

    /**
     * @var int
     */
    private $sourceTitleId;

    /**
     * @var int
     */
    private $industry;

    /**
     * @var int
     */
    private $field;

    /**
     * @var int
     */
    private $joblevel;

    /**
     * @var string
     */
    private $cleantitle;

    /**
     * @var \DateTime
     */
    private $createtime;

    /**
     * @var int
     */
    private $sourceCandidateId;

    /**
     * @var string
     */
    private $slugtitle;

    /**
     * @var int
     */
    private $prevWorkexp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return MasterTitle
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set source
     *
     * @param integer $source
     * @return MasterTitle
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return integer 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set sourceTitleId
     *
     * @param integer $sourceTitleId
     * @return MasterTitle
     */
    public function setSourceTitleId($sourceTitleId)
    {
        $this->sourceTitleId = $sourceTitleId;
    
        return $this;
    }

    /**
     * Get sourceTitleId
     *
     * @return integer 
     */
    public function getSourceTitleId()
    {
        return $this->sourceTitleId;
    }

    /**
     * Set industry
     *
     * @param integer $industry
     * @return MasterTitle
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return integer 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set field
     *
     * @param integer $field
     * @return MasterTitle
     */
    public function setField($field)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field
     *
     * @return integer 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set joblevel
     *
     * @param integer $joblevel
     * @return MasterTitle
     */
    public function setJoblevel($joblevel)
    {
        $this->joblevel = $joblevel;
    
        return $this;
    }

    /**
     * Get joblevel
     *
     * @return integer 
     */
    public function getJoblevel()
    {
        return $this->joblevel;
    }

    /**
     * Set cleantitle
     *
     * @param string $cleantitle
     * @return MasterTitle
     */
    public function setCleantitle($cleantitle)
    {
        $this->cleantitle = $cleantitle;
    
        return $this;
    }

    /**
     * Get cleantitle
     *
     * @return string 
     */
    public function getCleantitle()
    {
        return $this->cleantitle;
    }

    /**
     * Set createtime
     *
     * @param \DateTime $createtime
     * @return MasterTitle
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    
        return $this;
    }

    /**
     * Get createtime
     *
     * @return \DateTime 
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }

    /**
     * Set sourceCandidateId
     *
     * @param integer $sourceCandidateId
     * @return MasterTitle
     */
    public function setSourceCandidateId($sourceCandidateId)
    {
        $this->sourceCandidateId = $sourceCandidateId;
    
        return $this;
    }

    /**
     * Get sourceCandidateId
     *
     * @return integer 
     */
    public function getSourceCandidateId()
    {
        return $this->sourceCandidateId;
    }

    /**
     * Set slugtitle
     *
     * @param string $slugtitle
     * @return MasterTitle
     */
    public function setSlugtitle($slugtitle)
    {
        $this->slugtitle = $slugtitle;
    
        return $this;
    }

    /**
     * Get slugtitle
     *
     * @return string 
     */
    public function getSlugtitle()
    {
        return $this->slugtitle;
    }

    /**
     * Set prevWorkexp
     *
     * @param integer $prevWorkexp
     * @return MasterTitle
     */
    public function setPrevWorkexp($prevWorkexp)
    {
        $this->prevWorkexp = $prevWorkexp;
    
        return $this;
    }

    /**
     * Get prevWorkexp
     *
     * @return integer 
     */
    public function getPrevWorkexp()
    {
        return $this->prevWorkexp;
    }
}
