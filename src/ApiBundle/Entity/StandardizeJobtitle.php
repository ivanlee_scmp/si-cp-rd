<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StandardizeJobtitle
 */
class StandardizeJobtitle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cleantitle;

    /**
     * @var string
     */
    private $slugtitle;

    /**
     * @var int
     */
    private $industry;

    /**
     * @var int
     */
    private $field;

    /**
     * @var int
     */
    private $joblevel;

    /**
     * @var int
     */
    private $confidence;

    /**
     * @var int
     */
    private $jobtitleGroup;

    /**
     * @var int
     */
    private $deleted;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cleantitle
     *
     * @param string $cleantitle
     * @return StandardizeJobtitle
     */
    public function setCleantitle($cleantitle)
    {
        $this->cleantitle = $cleantitle;
    
        return $this;
    }

    /**
     * Get cleantitle
     *
     * @return string 
     */
    public function getCleantitle()
    {
        return $this->cleantitle;
    }

    /**
     * Set slugtitle
     *
     * @param string $slugtitle
     * @return StandardizeJobtitle
     */
    public function setSlugtitle($slugtitle)
    {
        $this->slugtitle = $slugtitle;

        return $this;
    }

    /**
     * Get slugtitle
     *
     * @return string
     */
    public function getSlugtitle()
    {
        return $this->slugtitle;
    }

    /**
     * Set industry
     *
     * @param integer $industry
     * @return StandardizeJobtitle
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return integer 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set field
     *
     * @param integer $field
     * @return StandardizeJobtitle
     */
    public function setField($field)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field
     *
     * @return integer 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set joblevel
     *
     * @param integer $joblevel
     * @return StandardizeJobtitle
     */
    public function setJoblevel($joblevel)
    {
        $this->joblevel = $joblevel;
    
        return $this;
    }

    /**
     * Get joblevel
     *
     * @return integer 
     */
    public function getJoblevel()
    {
        return $this->joblevel;
    }

    /**
     * Set confidence
     *
     * @param integer $confidence
     * @return StandardizeJobtitle
     */
    public function setConfidence($confidence)
    {
        $this->confidence = $confidence;
    
        return $this;
    }

    /**
     * Get confidence
     *
     * @return integer 
     */
    public function getConfidence()
    {
        return $this->confidence;
    }

    /**
     * Set jobtitleGroup
     *
     * @param integer $jobtitleGroup
     * @return StandardizeJobtitle
     */
    public function setJobtitleGroup($jobtitleGroup)
    {
        $this->jobtitleGroup = $jobtitleGroup;
    
        return $this;
    }

    /**
     * Get jobtitleGroup
     *
     * @return integer 
     */
    public function getJobtitleGroup()
    {
        return $this->jobtitleGroup;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return StandardizeJobtitle
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
