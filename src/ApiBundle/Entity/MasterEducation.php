<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasterEducation
 */
class MasterEducation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $masterTitleId;

    /**
     * @var int
     */
    private $standardizeTitleId;

    /**
     * @var int
     */
    private $edulevel;

    /**
     * @var string
     */
    private $institutename;

    /**
     * @var int
     */
    private $studyfield;

    /**
     * @var string
     */
    private $major;

    /**
     * @var int
     */
    private $achievedin;

    /**
     * @var string
     */
    private $source;

    /**
     * @var int
     */
    private $sourceEducationId;

    /**
     * @var \DateTime
     */
    private $createtime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set masterTitleId
     *
     * @param integer $masterTitleId
     * @return MasterEducation
     */
    public function setMasterTitleId($masterTitleId)
    {
        $this->masterTitleId = $masterTitleId;
    
        return $this;
    }

    /**
     * Get masterTitleId
     *
     * @return integer 
     */
    public function getMasterTitleId()
    {
        return $this->masterTitleId;
    }

    /**
     * Set standardizeTitleId
     *
     * @param integer $standardizeTitleId
     * @return MasterEducation
     */
    public function setStandardizeTitleId($standardizeTitleId)
    {
        $this->standardizeTitleId = $standardizeTitleId;
    
        return $this;
    }

    /**
     * Get standardizeTitleId
     *
     * @return integer 
     */
    public function getStandardizeTitleId()
    {
        return $this->standardizeTitleId;
    }

    /**
     * Set edulevel
     *
     * @param integer $edulevel
     * @return MasterEducation
     */
    public function setEdulevel($edulevel)
    {
        $this->edulevel = $edulevel;
    
        return $this;
    }

    /**
     * Get edulevel
     *
     * @return integer 
     */
    public function getEdulevel()
    {
        return $this->edulevel;
    }

    /**
     * Set institutename
     *
     * @param string $institutename
     * @return MasterEducation
     */
    public function setInstitutename($institutename)
    {
        $this->institutename = $institutename;
    
        return $this;
    }

    /**
     * Get institutename
     *
     * @return string 
     */
    public function getInstitutename()
    {
        return $this->institutename;
    }

    /**
     * Set studyfield
     *
     * @param integer $studyfield
     * @return MasterEducation
     */
    public function setStudyfield($studyfield)
    {
        $this->studyfield = $studyfield;
    
        return $this;
    }

    /**
     * Get studyfield
     *
     * @return integer 
     */
    public function getStudyfield()
    {
        return $this->studyfield;
    }

    /**
     * Set major
     *
     * @param string $major
     * @return MasterEducation
     */
    public function setMajor($major)
    {
        $this->major = $major;
    
        return $this;
    }

    /**
     * Get major
     *
     * @return string 
     */
    public function getMajor()
    {
        return $this->major;
    }

    /**
     * Set achievedin
     *
     * @param integer $achievedin
     * @return MasterEducation
     */
    public function setAchievedin($achievedin)
    {
        $this->achievedin = $achievedin;
    
        return $this;
    }

    /**
     * Get achievedin
     *
     * @return integer 
     */
    public function getAchievedin()
    {
        return $this->achievedin;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return MasterEducation
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set sourceEducationId
     *
     * @param integer $sourceEducationId
     * @return MasterEducation
     */
    public function setSourceEducationId($sourceEducationId)
    {
        $this->sourceEducationId = $sourceEducationId;
    
        return $this;
    }

    /**
     * Get sourceEducationId
     *
     * @return integer 
     */
    public function getSourceEducationId()
    {
        return $this->sourceEducationId;
    }

    /**
     * Set createtime
     *
     * @param \DateTime $createtime
     * @return MasterEducation
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    
        return $this;
    }

    /**
     * Get createtime
     *
     * @return \DateTime 
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }
}
