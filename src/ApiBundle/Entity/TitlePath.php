<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TitlePath
 */
class TitlePath
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $titleId;

    /**
     * @var int
     */
    private $standardizeId;

    /**
     * @var int
     */
    private $nextStandardizeId;

    /**
     * @var int
     */
    private $prevStandardizeId;

    /**
     * @var int
     */
    private $candidateid;

    /**
     * @var \DateTime
     */
    private $createtime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titleId
     *
     * @param integer $titleId
     * @return TitlePath
     */
    public function setTitleId($titleId)
    {
        $this->titleId = $titleId;
    
        return $this;
    }

    /**
     * Get titleId
     *
     * @return integer 
     */
    public function getTitleId()
    {
        return $this->titleId;
    }

    /**
     * Set standardizeId
     *
     * @param integer $standardizeId
     * @return TitlePath
     */
    public function setStandardizeId($standardizeId)
    {
        $this->standardizeId = $standardizeId;
    
        return $this;
    }

    /**
     * Get standardizeId
     *
     * @return integer 
     */
    public function getStandardizeId()
    {
        return $this->standardizeId;
    }

    /**
     * Set nextStandardizeId
     *
     * @param integer $nextStandardizeId
     * @return TitlePath
     */
    public function setNextStandardizeId($nextStandardizeId)
    {
        $this->nextStandardizeId = $nextStandardizeId;
    
        return $this;
    }

    /**
     * Get nextStandardizeId
     *
     * @return integer 
     */
    public function getNextStandardizeId()
    {
        return $this->nextStandardizeId;
    }

    /**
     * Set prevStandardizeId
     *
     * @param integer $prevStandardizeId
     * @return TitlePath
     */
    public function setPrevStandardizeId($prevStandardizeId)
    {
        $this->prevStandardizeId = $prevStandardizeId;
    
        return $this;
    }

    /**
     * Get prevStandardizeId
     *
     * @return integer 
     */
    public function getPrevStandardizeId()
    {
        return $this->prevStandardizeId;
    }

    /**
     * Set candidateid
     *
     * @param integer $candidateid
     * @return TitlePath
     */
    public function setCandidateid($candidateid)
    {
        $this->candidateid = $candidateid;
    
        return $this;
    }

    /**
     * Get candidateid
     *
     * @return integer
     */
    public function getCandidateid()
    {
        return $this->candidateid;
    }

    /**
     * Set createtime
     *
     * @param \DateTime $createtime
     * @return TitlePath
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    
        return $this;
    }

    /**
     * Get createtime
     *
     * @return \DateTime 
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }
}
