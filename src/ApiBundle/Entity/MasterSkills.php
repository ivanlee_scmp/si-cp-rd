<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasterSkills
 */
class MasterSkills
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sourceSkillsId;

    /**
     * @var int
     */
    private $source;

    /**
     * @var int
     */
    private $masterTitleId;

    /**
     * @var int
     */
    private $standardizeTitleId;

    /**
     * @var string
     */
    private $skills;

    /**
     * @var \DateTime
     */
    private $createtime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceSkillsId
     *
     * @param integer $sourceSkillsId
     * @return MasterSkills
     */
    public function setSourceSkillsId($sourceSkillsId)
    {
        $this->sourceSkillsId = $sourceSkillsId;
    
        return $this;
    }

    /**
     * Get sourceSkillsId
     *
     * @return integer 
     */
    public function getSourceSkillsId()
    {
        return $this->sourceSkillsId;
    }

    /**
     * Set source
     *
     * @param integer $source
     * @return MasterSkills
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return integer 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set masterTitleId
     *
     * @param integer $masterTitleId
     * @return MasterSkills
     */
    public function setMasterTitleId($masterTitleId)
    {
        $this->masterTitleId = $masterTitleId;
    
        return $this;
    }

    /**
     * Get masterTitleId
     *
     * @return integer 
     */
    public function getMasterTitleId()
    {
        return $this->masterTitleId;
    }

    /**
     * Set standardizeTitleId
     *
     * @param integer $standardizeTitleId
     * @return MasterSkills
     */
    public function setStandardizeTitleId($standardizeTitleId)
    {
        $this->standardizeTitleId = $standardizeTitleId;
    
        return $this;
    }

    /**
     * Get standardizeTitleId
     *
     * @return integer 
     */
    public function getStandardizeTitleId()
    {
        return $this->standardizeTitleId;
    }

    /**
     * Set skills
     *
     * @param string $skills
     * @return MasterSkills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    
        return $this;
    }

    /**
     * Get skills
     *
     * @return string 
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set createtime
     *
     * @param \DateTime $createtime
     * @return MasterSkills
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    
        return $this;
    }

    /**
     * Get createtime
     *
     * @return \DateTime 
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }
}
