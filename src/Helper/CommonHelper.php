<?php

namespace Helper;

use Doctrine\ORM\EntityManager;

class CommonHelper {
    protected $manager;
    protected $container;
    protected $request;
    protected $modelStatic;

    public function __construct($em, $cpdbem, $postgreem, $container, $req, $modelstatic = null)
    {
        $this->manager = $em;
        $this->cpdbmanager = $cpdbem;
        $this->postgremanager = $postgreem;
        $this->container = $container;
        $this->request = $req->getCurrentRequest();
        $this->modelStatic = $modelstatic;
    }

    public function setSystemMsg($message,$title="",$priority="success") {
        $msg['message']=rawUrlEncode($message);
        $msg['priority']=$priority;
        if(!empty($title)){
            $msg['title']=rawUrlEncode($title);
        }
        setcookie("system_msg",json_encode($msg), time()+10,"/");
    }

    public function cleanTitle($title) {
        $jobTitle = mb_convert_kana($title, 'r', 'UTF-8');

        //Remove punctuation mark related keyword (Need to handle first)
        $jobTitle = preg_replace('/\b[ ]*[^\x00-\x7F]+.*$/','', $jobTitle); //Remove all words after non-english character e.g. '電 話 續 約 員 - TeleSales 無 需 Cold Call' -> '電 話 續 約 員 - TeleSales'
        $jobTitle = preg_replace('/[^\x00-\x7F]+([^a-zA-Z0-9])*[^\x00-\x7F]/','', $jobTitle); //Remove all words after non-english character e.g. '電 話 續 約 員 - TeleSales' -> ' - TeleSales'
        $jobTitle = preg_replace('/\(.*?\)($|[ ])/','', $jobTitle); // Remove all round brackets (), e.g. 'AAA (test)', 'AAA'
        $jobTitle = preg_replace('/[()]/','', $jobTitle);
        $jobTitle = preg_replace('/（.*）/','', $jobTitle);
        $jobTitle = preg_replace('/[（）]/','', $jobTitle);
        $jobTitle = preg_replace('/\[.*\]/','', $jobTitle);
        $jobTitle = preg_replace('/[\[\]]/','', $jobTitle);
        $jobTitle = preg_replace('/\*/','', $jobTitle);
        $jobTitle = preg_replace('/－/',' - ', $jobTitle);
        $jobTitle = preg_replace('/–/',' - ', $jobTitle);
        $jobTitle = preg_replace('/–/',' - ', $jobTitle);
        $jobTitle = preg_replace('/\/[ ]*$/','', $jobTitle);
        $jobTitle = preg_replace('/^\/[ ]*/','', $jobTitle);
        $jobTitle = preg_replace('/\+$/','', $jobTitle);
        $jobTitle = preg_replace('/\//',' / ', $jobTitle);
        //Remove job description related keyword
        $jobTitle = preg_replace('/[0-9]+[ ]*[-]*[ ]*[0-9]*[ ]*(month|year|yr)[s]*[ ]*(contract[s]*)*/i','', $jobTitle);
        $jobTitle = preg_replace('/-.*shift.*/','', $jobTitle);
        $jobTitle = preg_replace('/ [0-9].*Positions[ ]*/','', $jobTitle);
        $jobTitle = preg_replace('/(permanent|temporary|contract|perm|temp|term)([ ]*|role[s]*|position[s]*|package[s]*|job[s]*)*/i','', $jobTitle);
        $jobTitle = preg_replace('/x [0-9]+/i','', $jobTitle);
        $jobTitle = preg_replace('/[xX][ ]*[0-9][0]*/','', $jobTitle);
        $jobTitle = preg_replace('/[ ]*urgent[ ]*/i','', $jobTitle);
        $jobTitle = preg_replace('/(part|full)[- ]*time[ ]*/i','', $jobTitle);
        $jobTitle = preg_replace('/[few]*[Few]*[ ]*post[s]*/','', $jobTitle);
        $jobTitle = preg_replace('/[rR]ef[\.]*[:]*.*$/','', $jobTitle);
        $jobTitle = preg_replace('/ [0-9]+[ ]*[eE]xp[.] */','', $jobTitle);
        $jobTitle = preg_replace('/^[tT]emp[\.]* /','', $jobTitle);
        //Remove salary related keyword
        $jobTitle = preg_replace('/(hk[d$]+|\$+)[\d,-]*[k]*[a-zA-Z0-9]*([ ]|$)/i','', $jobTitle);
        $jobTitle = preg_replace('/up to.*$/i','', $jobTitle);
        $jobTitle = preg_replace('/[ ]*per month[ ]*/i','', $jobTitle);
        $jobTitle = preg_replace('/!/','', $jobTitle);
        $jobTitle = preg_replace('/[ ]+I[i]*[v]*($|[ ]+)/i','', $jobTitle);
        $jobTitle = preg_replace('/ salary/i','', $jobTitle);
        //Remove job benefits related keyword
        $jobTitle = preg_replace('/\d.*[Dd]ay[s]*/','', $jobTitle);
        $jobTitle = preg_replace('/[- ]*[0-9]+[ ]*[dD]ay[s]*[ ]*[work]*[Work]*[ ]*[week]*/','', $jobTitle);
        $jobTitle = preg_replace('/working[-]*day[s]*/i','', $jobTitle);
        $jobTitle = preg_replace('/[nN]eeded/','', $jobTitle);
        //Remove location related keyword
        $jobTitle = preg_replace('/- hong kong[ ]*/i','', $jobTitle);
        $jobTitle = preg_replace('/[hH](ong )*[kK](ong)*( |$|-)([lL]isted)*/','', $jobTitle);
        $jobTitle = preg_replace('/(mainland( |-))*(greater( |-))*china/i','', $jobTitle);
        $jobTitle = preg_replace('/[mM]acau/','', $jobTitle);
        $jobTitle = preg_replace('/ [aA]t .*/','', $jobTitle);
        //Title correction
        $jobTitle = preg_replace('/(^|[ ]+)[iI]\.[tT][\.]*($|[ ]+)/i',' IT ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)[fF][ ]*\&+[ ]*[bB]($|[ ]+)/i',' F&B ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)[rR][ ]*\&+[ ]*[dD]($|[ ]+)/i',' R&D ', $jobTitle);
        $jobTitle = preg_replace('/[ ]+\&[ ]+/',' And ', $jobTitle);
        $jobTitle = preg_replace('/circa/i','', $jobTitle);
        $jobTitle = preg_replace('/ in the[ ]*.*$/i','', $jobTitle);
        $jobTitle = preg_replace('/ base[d]* in.*$/','', $jobTitle);
        $jobTitle = preg_replace('/[ ]+of$/','', $jobTitle);
        $jobTitle = preg_replace('/[ ]+\&$/','', $jobTitle);
        $jobTitle = preg_replace('/ [tT]ype[ ]*[0-9]/','', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)mgr[\.]*($|[ ]+)/i',' Manager ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)asst[\.]*($|[ ]+)/i',' Assistant ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)dept[\.]*($|[ ]+)/i',' Department ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)q\.*a\.*($|[ ]+)/i',' QA ', $jobTitle);
        $jobTitle = preg_replace("/(^|[ ]+)int\'l($|[ ]+)/i",' International ', $jobTitle);
        $jobTitle = preg_replace('/[ ]+for[ ].*/','', $jobTitle);
        $jobTitle = preg_replace('/[ ]+[iI]n[ ].*/','', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)[sS]r\.*($|[ ]+)/',' Senior ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)[jJ]r\.*($|[ ]+)/',' Junior ', $jobTitle);
        $jobTitle = preg_replace('/(^|[ ]+)[gG]\.*[ ]*[mM]\.*($|[ ]+)/i',' GM ', $jobTitle);
        //Remove punctuation mark related keyword (Need to handle at last)
        $jobTitle = preg_replace('/^(-|–)[ ]*/','', $jobTitle);
        $jobTitle = preg_replace('/(-|–)[ ]*$/','', $jobTitle);
        $jobTitle = preg_replace('/[\/]*[ ]*[Ww]ud/','', $jobTitle);
        $jobTitle = preg_replace('/<.*>/','', $jobTitle);
        $jobTitle = preg_replace('/(^| |\.)[\d]+(\.| |$|-)[\d]*/',' ', $jobTitle);
        $jobTitle = preg_replace('/\$/','', $jobTitle);
        $jobTitle = preg_replace('/ =( |$)/','', $jobTitle);
        $jobTitle = preg_replace('/ [0-9]+%( |$)/','', $jobTitle);
        $jobTitle = preg_replace('/_/',' ', $jobTitle);
        $jobTitle = preg_replace('/[ ]*\]/','', $jobTitle);
        // Connection words without word e.g. "Deputy and" => "Deputy"
        $jobTitle = preg_replace('/[ ]+of[ ]*$/i', '', $jobTitle);
        $jobTitle = preg_replace('/[ ]+and[ ]*$/i', '', $jobTitle);
        $jobTitle = preg_replace('/\s+/',' ', $jobTitle); //Remove all redundant space, e.g. "fsdf   sfdd" => "fsdf sfdd"
        // Trim all special char & space
        $jobTitle = preg_replace('/^[^A-Za-z0-9]*/',' ', $jobTitle);
        $jobTitle = preg_replace('/[^A-Za-z0-9]*$/',' ', $jobTitle);
        $jobTitle = trim($jobTitle);
        $jobTitle = explode(' ', $jobTitle);

        return implode(' ',$jobTitle);
    }

    public function cleanTitleToSlugifyTitle($cleantitle){
        $slugTitle = preg_replace('/(^|[ ]+)[gG]\.*[ ]*[mM]\.*($|[ ]+)/i', ' General Manager ', $cleantitle);
        $slugTitle = preg_replace('/(^|[ ]+)a[ ]*\/[ ]*c($|[ ]+)/i', ' Account ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)q\.*a\.*($|[ ]+)/i', ' Quality Assurance ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)q\.*c\.*($|[ ]+)/i', ' Quality Control ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)[iI][\.]*[tT][\.]*($|[ ]+)/i', ' Information Technology ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)[pP][\.]*[aA][\.]*($|[ ]+)/i', ' Personal Assistant ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)[pP][\.]*[rR][\.]*($|[ ]+)/i', ' Public Relation ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CRM($|[ ]+)/i',' Customer Relationship Management ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CSR($|[ ]+)/i',' Corporate Social Responsibility ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)R\&D($|[ ]+)/i', ' Research and Development ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)F\&B($|[ ]+)/i', ' Food and Beverage ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)HR($|[ ]+|\-+)/i', ' Human Resource ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)Dy\.($|[ ]+)/i', ' Deputy ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)EVP($|[ ]+)/i',' Executive Vice President ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)VP($|[ ]+)/i',' Vice President ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CEO($|[ ]+)/i',' Chief Executive Officer ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CFO($|[ ]+)/i',' Chief Financial Officer ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)COO($|[ ]+)/i',' Chief Operating Officer ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CTO($|[ ]+)/i',' Chief Technology Officer ', $slugTitle);
        $slugTitle = preg_replace('/(^|[ ]+)CIO($|[ ]+)/i',' Chief Information Officer ', $slugTitle);
        $slugTitle = preg_replace('/\-/',' ', $slugTitle);
        $slugTitle = preg_replace('/[^\w\-]+/',' ', $slugTitle);

        return preg_replace('/\-\-+/','-',implode('-',explode(' ',strtolower(trim($slugTitle)))));
    }

    public function slugifyTitle($title) {
        return self::cleanTitleToSlugifyTitle(self::cleanTitle($title));
    }

    public function id_to_industry($industryid){
        $industry= $this->cpdbmanager->getRepository("DataBundle:ClsIndustry")->findOneById($industryid);
        if($industry){
            return $industry->getIndustrydesc();
        }else{
            return null;
        }
    }

    // Todo: Temporary use only, should be deprecated after merging mysql and postgresql database
    public function postgreStandardizeTitleToMysqlStandardizeId($pgStandardizeTitle){
        $result= $this->cpdbmanager->createQuery('SELECT sj.id FROM DataBundle:ClsStandardizeJobtitle sj WHERE sj.slug = :pgStandardizeTitle')
            ->setMaxResults(1)
            ->setParameter('pgStandardizeTitle', self::slugifyTitle($pgStandardizeTitle))
            ->getResult();
        if(empty($result)){
            return null;
        }else{
            return $result[0]['id'];
        }
    }

    public function id_to_slugtitle($id){
        $result= $this->postgremanager->createQuery('SELECT sj.slugtitle FROM ApiBundle:StandardizeJobtitle sj WHERE sj.id = :id')
            ->setMaxResults(1)
            ->setParameter('id', $id)
            ->getResult();
        if(empty($result)){
            return null;
        }else{
            return $result[0]['slugtitle'];
        }
    }
}